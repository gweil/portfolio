# Simple portfolio.

This template only uses HTML and CSS, not JavaScript. The fundamental reason is that I wanted to keep the project as light as possible, and the features brought by JS don't add any value to the portfolio content which is your unique experiences and your work.

Have fun adapting the code to your needs and modifying the color palette to make your portfolio glamorous ! :wink: :sparkles: :yellow_heart:

---

You can check my personal portfolio [here](https://gweil.neocities.org) made with this template.

---
